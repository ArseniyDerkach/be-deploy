const express = require("express");
const cors = require("cors");
const { users } = require("../mock/users");
const { getUsers, addUser } = require("../db");

const app = express();

app.use(cors());
app.use(express.json());

const port = 8080;

app.get("/", function (req, res) {
  res.send("lorem ipsum12345");
});

app.get("/posts", function (req, res) {
  res.status(401).send("posts not found");
});

app.get("/users", async function (req, res) {
  const data = await getUsers();
  res.send(JSON.stringify(data));
});

app.post("/user", async function (req, res) {
  const user = req.body;
  const newUser = await addUser(user);
  console.log(newUser);
  users.push(user);
  res.send(user);
});

app.listen(port, () => {
  console.log(`server started at http://localhost/${port}`);
});
