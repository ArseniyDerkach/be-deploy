const users = [
  {
    id: 1,
    firstName: "John",
    lastName: "Doe",
    age: 30,
  },
  {
    id: 2,
    firstName: "Jane",
    lastName: "Doe",
    age: null,
  },
];

exports.users = users;
