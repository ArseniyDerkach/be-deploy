const { MongoClient, ServerApiVersion } = require("mongodb");
const uri =
  "mongodb+srv://arseniiderkach:bPEjcYr1ZYyNuEzj@cluster0.lfxlcmo.mongodb.net/?retryWrites=true&w=majority";
// Create a MongoClient with a MongoClientOptions object to set the Stable API version
const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  },
});
async function run() {
  try {
    // Connect the client to the server	(optional starting in v4.7)
    await client.connect();
    const db = await client.db("userlist");
    const usersCollection = await db.collection("users");
    // Send a ping to confirm a successful connection
    // await client.db("admin").command({ ping: 1 });
    // await client.db("userlist").createCollection("users");
    const data = {
      id: Date.now(),
      firstName: "Jack",
      lastName: "Daniels",
      age: 35,
    };
    // await client.db("userlist").collection("users").insertOne(data);
    // const elements = await client
    //   .db("userlist")
    //   .collection("users")
    //   .find({ age: 40 });
    // await elements.forEach((doc) => console.log(doc));
    // const element = await client
    //   .db("userlist")
    //   .collection("users")
    //   .findOne({ age: 35 });
    // console.log(element);
    // const elementToDelete = await usersCollection.deleteOne({
    //   lastName: "Daniels",
    // });
    // console.log(elementToDelete);
    // console.log(elementToDelete.deletedCount);
    // const result = await usersCollection.deleteMany({ age: 40 });
    // console.log(result);
    // const result = await usersCollection.updateOne(
    //   { age: 40 },
    //   { $set: { firstName: "Jim", lastName: "Beam" } },
    //   { upsert: true }
    // );
    // console.log(result);
    // const updateMany = await usersCollection.updateMany(
    //   { age: 35 },
    //   { $set: { lastName: "Daniels XO" } }
    // );
    // console.log(updateMany);
    // const replace = await usersCollection.replaceOne(
    //   { age: 40 },
    //   { firstName: "John", lastName: "Doe" }
    // );
    // console.log(replace);
  } finally {
    // Ensures that the client will close when you finish/error
    await client.close();
  }
}
// run().catch(console.dir);

async function getUsers() {
  await client.connect();
  const db = await client.db("userlist");
  const usersCollection = await db.collection("users");
  const result = await usersCollection.find({}).toArray();
  await client.close();
  return result;
}
async function addUser(user) {
  await client.connect();
  const db = await client.db("userlist");
  const usersCollection = await db.collection("users");
  const newUser = await usersCollection.insertOne(user);
  await client.close();
  return newUser;
}
// TODO: винести код з 77-79 та 81 рядку в окремий метод(?)
// TODO: спробувати це написати через метод, який буде приймати колбек-функцію
exports.getUsers = getUsers;
exports.addUser = addUser;
